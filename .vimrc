source $HOME/dotfiles/vimrcfiles/vundle        " pluginの読込
source $HOME/dotfiles/vimrcfiles/general       " 基本設定
source $HOME/dotfiles/vimrcfiles/encoding      " エンコーディング
source $HOME/dotfiles/vimrcfiles/keybind       " キーバインド
source $HOME/dotfiles/vimrcfiles/look_and_feel " 外観
source $HOME/dotfiles/vimrcfiles/search        " 検索
source $HOME/dotfiles/vimrcfiles/neocomplcache " プラグイン設定
